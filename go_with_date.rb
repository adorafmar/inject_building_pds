require 'csv'
require 'pry'
require "net/https"
require 'httparty'
require 'aws-sdk-s3'

ONE_SECOND = 1000;
ONE_MINUTE = 60*ONE_SECOND;
TWO_MINUTES = 2*60*ONE_SECOND;
ONE_HOUR = 60*ONE_MINUTE;
ONE_DAY = 24*ONE_HOUR;

# unix time for PD data
specific_time = true
if specific_time
	CURRENT_TIME = Time.new(2022, 6, 21, 1, 0, 0).to_i * 1000
else
	CURRENT_TIME = Time.now.to_i * 1000
end
output_file = "building_tags_"+Time.at(CURRENT_TIME/1000).to_s.gsub("-",".").gsub(" ","_").gsub(":",".").gsub("_.","_tz")+".csv"

$from = CURRENT_TIME - ONE_DAY; 
$to   = CURRENT_TIME

def object_downloaded?(s3_client, bucket_name, object_key, local_path)
  s3_client.get_object(
    response_target: local_path,
    bucket: bucket_name,
    key: object_key
  )
rescue StandardError => e
  puts "Error getting object: #{e.message}"
end

def get_a_page tag_pointer
	puts "tag_pointer: "+tag_pointer.to_s
	page_of_tags = $tags[tag_pointer...tag_pointer+$page_size].join(",")
	#  url = "https://10.101.206.21/webapi/api/trend/multitag?tags="+page_of_tags+"&start="+$from.to_s+"&end="+$to.to_s+"&interval=3600000"
	url = "https://10.101.206.21/webapi/api/snapshot/multitag?tags="+page_of_tags+"&time="+$to.to_s
	puts url
	response = HTTParty.get(url, :verify => false)
	if response["message"] == "SUCCESS" then
		data = response["data"]
		data.each do |datum|
			# {"Tag"=>"CMC_A0441_PD", "T"=>1649826005680, "V"=>400.73, "Q"=>192}
			one_record = [];
			one_record << $to
			one_record << datum["Tag"]
			one_record << datum["V"]
			$day_file  << one_record
		end
	elsif
		puts "response error: " + response["message"].to_s
	end
end

def object_uploaded?(s3_client, bucket_name, object_key, body)
  response = s3_client.put_object(
    bucket: bucket_name,
    key: object_key,
    body: body
  )
  if response.etag
    return true
  else
    return false
  end
rescue StandardError => e
  puts "Error uploading object: #{e.message}"
  return false
end

region = 'us-east-1'
s3_client = Aws::S3::Client.new(region: region)
bucket_name = 'utpower'

object_key = 'building_tags.csv'
local_path = object_key

if object_downloaded?(s3_client, bucket_name, object_key, local_path)
	puts "Object '#{object_key}' in bucket '#{bucket_name}' downloaded to '#{local_path}'."
	
	$tags = CSV.read(local_path)
	if $tags.length != 0

		$page_size = 1
		n_tags = $tags.length
		tag_pointer = 0
		$day_file = [];
		$day_file << ["time","tag","value"]
		while tag_pointer < n_tags do
			get_a_page tag_pointer
			tag_pointer = tag_pointer + $page_size;
		end
		body = $day_file.map { |c| c.join(",") }.join("\n")
		puts body

		object_key = 'telemetry-buildings_historian_dynamic/'+output_file
		if object_uploaded?(s3_client, bucket_name, object_key, body)
			puts "Object '#{object_key}' uploaded to bucket '#{bucket_name}'."
		else
			puts "Object '#{object_key}' not uploaded to bucket '#{bucket_name}'."
		end
	else
		puts "list is empty"
	end
else
  puts "Object '#{object_key}' in bucket '#{bucket_name}' not downloaded."
end